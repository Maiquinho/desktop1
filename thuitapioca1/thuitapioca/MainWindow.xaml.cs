﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace thuitapioca
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
        string usuario, senha;
       int i=0;
        public MainWindow()
        {
            InitializeComponent();
            Txtlogin.Focus(); 
        }

        private void Txtlogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key==Key.Enter)
            {
                usuario = Txtlogin.Text;
                Txtsenha.IsEnabled = true;
                Txtsenha.Focus();
            }
        }

        private void Txtsenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key==Key.Enter)
            {
                senha = Txtsenha.Password;
                Btnentrar.IsEnabled = true;
                Btnentrar.Focus();
            }
        }

        

        private void Btnentrar_Click(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();
            MySqlCommand comm = new MySqlCommand("SELECT * FROM funcionario WHERE email = @usuario AND senha =@senha AND statusFuncionario='ATIVO'", bd.conexao);
            comm.Parameters.Clear();
            comm.Parameters.Add("@usuario", MySqlDbType.String).Value = Txtlogin.Text;
            comm.Parameters.Add("@senha", MySqlDbType.String).Value = Txtsenha.Password;

            try
            {

                comm.CommandType = CommandType.Text;
                MySqlDataReader dr = comm.ExecuteReader();
                dr.Read();
                //NAO ESQUECER, ele sempre pega um campo acima da ordem do banco pois começa a partir do 0
                acesso.nivelAcesso = dr.GetString(13);
                acesso.empresaAcesso = dr.GetString(2);
                //acesso.empresaAcesso = dr.GetString(17);

                Btnentrar.IsEnabled = false;
                Thread.Sleep(10);
                i = 0;
                Task.Run(() =>
                {
                    while (i < 100)
                    {
                        i++;
                        Thread.Sleep(50);
                        this.Dispatcher.Invoke(() => //usar para att imediata obrigatoria

                        {
                            //PgbLogin.Value = i;<-animacaoAPLICARNAOESQUECER

                            while (i == 100)
                            {
                                usuario = acesso.empresaAcesso;

                                if (acesso.nivelAcesso == "ADM")
                                {
                                    Hide();
                                   menu frm = new menu();
                                    frm.Show();
                                    frm.BtnFuncionario.IsEnabled = true;
                                    frm.LblLogado.Content = usuario;
                                    
                                    break;
                                }
                                else if (acesso.nivelAcesso == "FUNCIONARIO")
                                {
                                    Hide();
                                    menu frm = new menu();
                                    frm.Show();
                                    frm.LblLogado.Content = usuario;
                                    frm.BtnFuncionario.IsEnabled = false;
                                    
                                    break;

                                }

                                else if (acesso.nivelAcesso == "GERENTE")
                                {

                                    menu frm = new menu();
                                    frm.Show();
                                    frm.LblLogado.Content = usuario;
                                    frm.BtnFuncionario.IsEnabled = false;
                                    break;
                                }
                            }
                        });
                    }
                });
            }
            catch
            {
                MessageBox.Show("Favor preencher o login ou a senha corretamente!");
                Txtlogin.Clear();
                Txtsenha.Clear();
                Txtlogin.Focus();
                Txtsenha.IsEnabled = false;
                Btnentrar.IsEnabled = false;
            }
        

    }
    }
}
