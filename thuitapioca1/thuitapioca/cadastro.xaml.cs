﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace thuitapioca
{
    /// <summary>
    /// Lógica interna para cadastro.xaml
    /// </summary>
    public partial class cadastro : Window
    {
        string  codigo; 
        public cadastro()
        {
            InitializeComponent();
            CmbEmpresa.Focus();

        }

        private void CmbEmpresa_KeyDown(object sender, KeyEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();
            MySqlCommand comm = new MySqlCommand("SELECT * FROM empresa WHERE nome = ?", bd.conexao);
            comm.Parameters.Clear();
            comm.Parameters.Add("@nome", MySqlDbType.String).Value = CmbEmpresa.Text;

            comm.CommandType = CommandType.Text;

            MySqlDataReader dr = comm.ExecuteReader();
            dr.Read();
            codigo = dr.GetString(0);

            TxtNome.IsEnabled = true;
            TxtNome.Focus();
        }
    
        private void CmbEmpresa_Loaded(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT idEmpresa, nome FROM empresa";
            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            CmbEmpresa.DisplayMemberPath = "nome";
            CmbEmpresa.ItemsSource = dt.DefaultView;
        }

        private void TxtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key==Key.Enter)
            {
                TxtCpf.IsEnabled = true;
                TxtCpf.Focus();
            }
        }

        private void TxtCpf_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtTelefone.IsEnabled = true;
                TxtTelefone.Focus();
            }
        }

        private void TxtTelefone_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtCelular.IsEnabled = true;
                TxtCelular.Focus();
            }
        }

        private void TxtCelular_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtEmail.IsEnabled = true;
                TxtEmail.Focus();
            }
        }

        private void TxtEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtEndereco.IsEnabled = true;
                TxtEndereco.Focus();
            }
        }

        private void TxtEndereco_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtCep.IsEnabled = true;
                TxtCep.Focus();
            }
        }

        private void TxtCep_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtUF.IsEnabled = true;
                TxtUF.Focus();
            }
        }

        private void TxtUF_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtLogin.IsEnabled = true;
                TxtLogin.Focus();
            }
        }

        private void TxtLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtNumero.IsEnabled = true;
                TxtNumero.Focus();
            }
        }



        private void TxtNumero_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter)
            {
                TxtComplemento.IsEnabled = true;
                TxtComplemento.Focus();
            }
        }

        private void TxtComplemento_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter)
            {
                TxtBairro.IsEnabled = true;
                TxtBairro.Focus();
            }
        }

        private void TxtBairro_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter)
            {
                TxtCidade.IsEnabled = true;
                TxtCidade.Focus();
            }
        }

        private void TxtCidade_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter)
            {
                TxtCargo.IsEnabled = true;
                TxtCargo.Focus();
            }
        }

        private void TxtCargo_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter)
            {
                TxtDataDemissao.IsEnabled = true;
                TxtDataDemissao.Focus();
            }
        }

        private void TxtDataDemissao_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter)
            {
                TxtDataAdmissao.IsEnabled = true;
                TxtDataAdmissao.Focus();
            }
        }

        private void TxtDataAdmissao_KeyDown(object sender, KeyEventArgs e)
        {

            TxtDataCadFuncionario.Text = DateTime.Now.ToShortDateString();
            ChkStatusFuncionario.IsEnabled = true;
            MessageBox.Show("Não esqueça de preencher o status do cliente");

            BtnSalvar.IsEnabled = true;
            BtnSalvar.Focus();
        }


        private void BtnNovo_Click(object sender, RoutedEventArgs e)
        {
            BtnLimpar.IsEnabled = false;
            BtnNovo.IsEnabled = false;
            CmbEmpresa.IsEnabled = true;
            CmbEmpresa.Focus(); 

        }


        private void BtnLimpar_Click(object sender, RoutedEventArgs e)
        {
            CmbEmpresa.SelectedIndex = -1;
            TxtNome.Clear();
            TxtEndereco.Clear();
            TxtNumero.Clear();
            TxtComplemento.Clear();
            TxtBairro.Clear();
            TxtCidade.Clear();
            TxtUF.Clear();
            TxtCep.Clear();
            TxtTelefone.Clear();
            TxtCelular.Clear();
            TxtEmail.Clear();
            TxtCargo.Clear();
            TxtDataAdmissao.Clear();
            TxtDataDemissao.Clear();
            TxtSenha.Clear();
            TxtDataCadFuncionario.Clear();
            ChkStatusFuncionario.IsChecked = false;
            //FIM DA LIMPEZA
            //desabilitando os botões
            TxtNome.IsEnabled = false;
            TxtEndereco.IsEnabled = false;
            TxtNumero.IsEnabled = false;
            TxtComplemento.IsEnabled = false;
            TxtBairro.IsEnabled = false;
            TxtCidade.IsEnabled = true;
            TxtUF.IsEnabled = false;
            TxtCep.IsEnabled = false;
            TxtTelefone.IsEnabled = false;
            TxtCelular.IsEnabled = false;
            TxtEmail.IsEnabled = false;
            TxtCargo.Clear();
            TxtDataAdmissao.IsEnabled = false;
            TxtDataDemissao.IsEnabled = false;
            TxtSenha.IsEnabled = false;
            TxtDataCadFuncionario.IsEnabled = false;
            ChkStatusFuncionario.IsEnabled = false;

            BtnSalvar.IsEnabled = false;
            BtnLimpar.IsEnabled = false;//desabilitado para voltar somente dps de se aperta NOVO e enter no primeiro item>NOME<
            //Unico botão ativo.
            BtnNovo.IsEnabled = true;
            BtnNovo.Focus();

        }

        private void BtnSalvar_Click(object sender, RoutedEventArgs e)

        {
            string status;
            DateTime DataCadastro = DateTime.Today;

            Banco bd = new Banco();
            bd.Conectar();
            if (ChkStatusFuncionario.IsChecked == true)
            {
                status = "ATIVO";

                string inserir = "INSERT INTO funcionario(idEmpresa,nome,endereco,numero,complemento,bairro,cidade,uf,cep,telefone,celular,email,cargo,dataAdmissao,dataDemissao,senha,statusFuncionario,dataCadFuncionario)VALUES('"
                 + codigo + "','"
                 + TxtNome.Text + "','"
                 + TxtEndereco.Text + "','"
                 + TxtNumero.Text + "','"
                 + TxtComplemento.Text + "','"
                 + TxtBairro.Text + "','"
                 + TxtCidade.Text + "','"
                 + TxtUF.Text + "','"
                 + TxtCep.Text + "','"
                 + TxtTelefone.Text + "','"
                 + TxtCelular.Text + "','"
                 + TxtEmail.Text + "','"
                 + TxtCargo.Text + "','"
                 + TxtDataAdmissao.Text + "','"
                 + TxtDataDemissao.Text + "','"
                 + TxtSenha.Text + "','"
                 + status + "','"
                 + DataCadastro.ToString("yyyy-MM--dd") + "')";


                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }
            else
            {
                status = "INATIVO";

                string inserir = "INSERT INTO funcionario(idEmpresa,nome,endereco,numero,complemento,bairro,cidade,uf,cep,telefone,celular,email,cargo,dataAdmissao,dataDemissao,senha,statusFuncionario,dataCadFuncionario)VALUES('"
                  + codigo + "','"
                  + TxtNome.Text + "','"
                  + TxtEndereco.Text + "','"
                  + TxtNumero.Text + "','"
                  + TxtComplemento.Text + "','"
                  + TxtBairro.Text + "','"
                  + TxtCidade.Text + "','"
                  + TxtUF.Text + "','"
                  + TxtCep.Text + "','"
                  + TxtTelefone.Text + "','"
                  + TxtCelular.Text + "','"
                  + TxtEmail.Text + "','"
                  + TxtCargo.Text + "','"
                  + TxtDataAdmissao.Text + "','"
                  + TxtDataDemissao.Text + "','"
                  + TxtSenha.Text + "','"
                  + status + "','"
                  + DataCadastro.ToString("yyyy-MM--dd") + "')";

                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }

            bd.Desconectar();
            MessageBox.Show("Funcionario cadastrado com sucesso!!", "Cadastro de funcionarios");

        }



        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
