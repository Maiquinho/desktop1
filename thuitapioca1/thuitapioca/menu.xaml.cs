﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace thuitapioca
{
    /// <summary>
    /// Lógica interna para menu.xaml
    /// </summary>
    public partial class menu : Window
    {
        public menu()
        {
            InitializeComponent();
            BtnFuncionario.IsEnabled = true;
        }

        private void BtnFuncionario_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            cadastro frm = new cadastro();
            frm.Show();
            this.Close();
            
        }

        
    }
}
